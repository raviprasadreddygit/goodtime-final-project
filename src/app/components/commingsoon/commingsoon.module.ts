import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {FormsModule , ReactiveFormsModule} from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { SwiperModule } from 'ngx-useful-swiper';

import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { MyOwnCustomMaterialModule } from 'src/app/app.material.module';

import { ComingsooncompComponent } from './comingsooncomp/comingsooncomp.component';

export const routes: Routes = [
  // { path: '', component: ComingsooncompComponent },
  { path: 'comingsoon', component: ComingsooncompComponent }
//   { path: 'signup', component: SignupComponent }
];

@NgModule({
  declarations: [  ComingsooncompComponent],
  imports: [
    CommonModule,
    MyOwnCustomMaterialModule,
    AngularFontAwesomeModule,
    FormsModule,
    FlexLayoutModule,
    SwiperModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)

  ]

  // imports: [
  //   RouterModule.forChild(routes)
  // ],
})
export class CommingSoonModule { }
