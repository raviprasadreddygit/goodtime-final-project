import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition,
  animate, group, query, stagger, keyframes
} from '@angular/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comingsooncomp',
  templateUrl: './comingsooncomp.component.html',
  styleUrls: ['./comingsooncomp.component.css'] ,
  animations: [
    trigger('slideInOut', [
      state('in', style({
        overflow: 'auto',
        height: 'auto',
        width: '100%',
      })),
      state('out', style({
        opacity: '0',
        overflow: 'auto',
        height: '0px',
        width: '0px'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ])
  ]
})
export class ComingsooncompComponent implements OnInit {


  constructor( private router: Router) { }

  showOnedropDown = false;

  showTwodropDown = false;

  showThreedropDown = false;
  OneInVal = '';
  TwoInVal = '';
  ThreeInVal = '';

  config: any = {
    autoplay: 3000,
    pagination: {
    el: '.swiper-pagination',
    },
    initialSlide: 4,
    slidesPerView: 4,
    paginationClickable: true,
    navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
    },
    spaceBetween: 10,
    breakpoints: {
      640: {
           slidesPerView: 1,
          },
      768: {
        slidesPerView: 2,
      },
      992: {
        slidesPerView: 3,
      }
    }
};

helpMenuOpen: string;

oneDetails = false;
twoDetails = false;
threeDetails = false;
fourDetails = false;
fiveDetails = false;
sixDetails = false;
sevenDetails = false;

  ngOnInit() {
    this.helpMenuOpen = 'out';
  }

  toggleHelpMenu(showVal): void {
    if (showVal === '1') {
      this.oneDetails = true;
      this.twoDetails = false;
      this.threeDetails = false;
      this.fourDetails = false;
      this.fiveDetails = false;
      this.sixDetails = false;
      this.sevenDetails = false;

    } else if (showVal === '2') {
      this.twoDetails = true;
  
      this.oneDetails = false;
      this.threeDetails = false;
      this.fourDetails = false;
      this.fiveDetails = false;
      this.sixDetails = false;
      this.sevenDetails = false;

    } else if (showVal === '3') {
      this.threeDetails = true;
  
      this.oneDetails = false;
      this.twoDetails = false;
      this.fourDetails = false;
      this.fiveDetails = false;
      this.sixDetails = false;
      this.sevenDetails = false;

    } else if (showVal === '4') {
      this.fourDetails = true;

      this.oneDetails = false;
      this.twoDetails = false;
      this.threeDetails = false;
      this.fiveDetails = false;
      this.sixDetails = false;
      this.sevenDetails = false;

    } else if (showVal === '5') {
      this.fiveDetails = true;

      this.fourDetails = false;

      this.oneDetails = false;
      this.twoDetails = false;
      this.threeDetails = false;
      this.sixDetails = false;
      this.sevenDetails = false;

    } else if (showVal === '6') {
      this.sixDetails = true;

      this.fiveDetails = false;

      this.fourDetails = false;

      this.oneDetails = false;
      this.twoDetails = false;
      this.threeDetails = false;
      this.sevenDetails = false;

    } else if (showVal === '7') {
      this.sevenDetails = true;

      this.fiveDetails = false;
      this.fourDetails = false;
      this.oneDetails = false;
      this.twoDetails = false;
      this.threeDetails = false;
      this.sixDetails = false;

    }
    this.helpMenuOpen = this.helpMenuOpen === 'out' ? 'in' : 'out';
  }


  onsaleDetails() {
    this.router.navigate(['/onsale/onsaledetails']);
  }

  showOneDiv() {
    this.showOnedropDown = ! this.showOnedropDown ;
  }

  showTwoDiv() {
    this.showTwodropDown = ! this.showTwodropDown ;
  }

  showThreeDiv(){
    this.showThreedropDown = ! this.showThreedropDown ; 
  }

  onetoggle(val) {
    // console.log(val);
    this.OneInVal = val;
        // console.log(this.OneInVal);
  }

  twotoggle(val) {
    console.log(val);
    this.TwoInVal = val;
        // console.log(this.OneInVal);
  }

  threetoggle(val) {
    console.log(val);
    this.ThreeInVal = val;
        // console.log(this.OneInVal);
  }

  SearchPlace() {
    this.router.navigate(['/onsale/seatselection']);

  }


  // /onsale/seatselection
}
