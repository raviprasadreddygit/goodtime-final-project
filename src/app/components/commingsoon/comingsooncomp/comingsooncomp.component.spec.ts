import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComingsooncompComponent } from './comingsooncomp.component';

describe('ComingsooncompComponent', () => {
  let component: ComingsooncompComponent;
  let fixture: ComponentFixture<ComingsooncompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComingsooncompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComingsooncompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
