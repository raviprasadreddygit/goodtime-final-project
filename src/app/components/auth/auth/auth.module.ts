import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {FormsModule , ReactiveFormsModule} from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { NgxCaptchaModule } from 'ngx-captcha';

import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { MyOwnCustomMaterialModule } from 'src/app/app.material.module';

import { LoginComponent } from '../login/login.component';
import { SignupComponent } from '../signup/signup.component';

export const routes: Routes = [
  // { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent }
];

@NgModule({
  declarations: [ SignupComponent , LoginComponent],
  imports: [
    CommonModule,
    MyOwnCustomMaterialModule,
    AngularFontAwesomeModule,
    FlexLayoutModule,
    FormsModule,
    NgxCaptchaModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)

  ]

  // imports: [
  //   RouterModule.forChild(routes)
  // ],
})
export class AuthModule { }
