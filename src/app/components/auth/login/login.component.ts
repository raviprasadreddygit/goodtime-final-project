import { Component, OnInit, Inject, Input } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material';

import { SignupComponent } from '../signup/signup.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  // description:string;

  rembermeok = false;
  rembermenotok = true;

  submitted = false;

  showDiv = false;


  // loginemail: any;
  // loginpassword: any;
  // loginremeberme: any;

  // constructor(
  //   private fb: FormBuilder,
  //   private dialogRef: MatDialogRef<LoginComponent>,
  //   @Inject(MAT_DIALOG_DATA) data) {
  //   this.loginemail = data.loginemail;
  //   this.loginpassword = data.loginpassword;
  //   this.loginremeberme = data.loginremeberme;
  // }


  constructor(@Inject(MAT_DIALOG_DATA) public data: any ,
              private dialogRef: MatDialogRef<LoginComponent>,
              private fb: FormBuilder,
              public dialog: MatDialog,
              private  router: Router) {
                // console.log(data);
              }
  ngOnInit() {


    this.loginForm = this.fb.group({
      // loginremeberme: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      RememberMe : ['']
  });


  //   this.loginForm = this.fb.group({
  //     loginemail: ['', Validators.required],
  //     loginpassword: ['', [Validators.required, Validators.minLength(6)]],
  //     loginremeberme: ['', [Validators.required, Validators.email]],
  // });

  // this.loginForm = new FormGroup({
  //   // tslint:disable-next-line
  //   loginemail: new FormControl('', [Validators.required ]),
  //   loginpassword: new FormControl('', [Validators.required]),
  //   loginremeberme : new FormControl('')
  // });


    // this.loginForm = new FormGroup({
    //   loginemail: new FormControl(''),
    //   loginpassword: new FormControl(''),
    //   loginremeberme: new FormControl('')
    // });

  }

  get f() { return this.loginForm.controls; }

  loginFormData() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
  }
    // console.log(this.loginForm.value);
    this.dialogRef.close(this.loginForm.value);
    // console.log('ok working.. login');
}

  changeRembermeStatus() {
    this.rembermeok = !this.rembermeok;
    this.rembermenotok = !this.rembermenotok;
  }

//   close() {
//     this.dialogRef.close();
// }



opensignUppage() {
  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;
  dialogConfig.data = {
      id: 1,
      title: 'Angular For Beginners'
  };

  this.dialogRef.close();

  // this.dialogRef.close();

  // const dialogRef =
  this.dialog.open(SignupComponent, dialogConfig);

  // const dialogRef = this.dialog.open(SignupComponent, dialogConfig);

  // console.log('ok test signup');

  // dialogRef.afterClosed().subscribe(result => {
  //   // alert("response: " + result);
  //   console.log(result);
  //   if (result == undefined || result == null || result == ''){
  //     // this.showProfile = false;
  //     // this.showlogs = true;
  //   } else {
  //     // this.showProfile = true;
  //   //  this.showlogs = false;
  //   }
  // });
}


}
