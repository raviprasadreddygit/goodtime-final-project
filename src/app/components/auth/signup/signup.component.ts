

import { Component, OnInit, VERSION, ViewChild, Inject, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';


@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  // encapsulation: ViewEncapsulation.None
})
export class SignupComponent implements OnInit  {


  @Output() profileshowOutPut = new EventEmitter<any>();

  showProfile = true;

  showlogs = false;


  private shouldSizeUpdate: boolean;

  protected registerForm: FormGroup;

  modalTitle: string;

  submitted = false;


  passwordmatch = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any ,
              private dialogRefSignUp: MatDialogRef<SignupComponent>,
              private fb: FormBuilder,
              private router: Router) {
    this.modalTitle = data.title;
    this.shouldSizeUpdate = data.shouldSizeUpdate;
    // console.log(data);
  }


  public breakpoint: number; // Breakpoint observer code
  // tslint:disable-next-line:no-inferrable-types
  wasFormChanged = false;

  // constructor(
  //   private fb: FormBuilder,
  //   public dialog: MatDialog
  // ) { }

  public ngOnInit(): void {
    // this.dialogRef.updateSize('40%', '100%');

    this.registerForm = this.fb.group({
      signupnemail: ['', [Validators.required , Validators.email]],
      signupnConfirmemail: ['', Validators.required],
      signuppassword: ['', [Validators.required , Validators.minLength(6)]],
      signupConfirmpassword : ['', Validators.required],
      dateofbirth : ['', Validators.required],
      username: ['', Validators.required],
      regrobot: [''],
      reggender: ['', Validators.required]
    });
    // this.breakpoint = window.innerWidth <= 600 ? 1 : 2; // Breakpoint observer code
  }

      // convenience getter for easy access to form fields
      get f() { return this.registerForm.controls; }



  signUpFormData() {

    // tslint:disable-next-line:no-debugger
    debugger;
    this.passwordmatch = false;
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
  }
  //   if (this.registerForm.value.signuppassword !== this.registerForm.value.signupConfirmpassword) {
  //   this.passwordmatch = true;
  // } else {
  //   this.passwordmatch = false;
  // }

    console.log(this.registerForm.value);
    this.dialogRefSignUp.close(this.registerForm.value);

    // console.log('signUp Data');
    // debugger;

    // const logindialogRef = this.dialog.open(LoginComponent, dialogloginConfig);

    this.dialogRefSignUp.afterClosed().subscribe(result => {
      // alert("response: " + result);
      if (result == undefined || result == null || result == ''){
        // this.showProfile = false;
        // console.log(result);
        // console.log('signup is  is added');
        // this.showlogs = true;
    
      } else {
        // console.log(result);
        // console.log('signup  is added');
        this.profileshowOutPut.emit('message');

        // console.log(this.profileshowOutPut);

        const navigationExtras: NavigationExtras = {
          queryParams: {
              showProfile: true
          }

        };
        this.router.navigate(['/onsale/onsale'], navigationExtras );

        // this.showProfile = true;
        // this.showlogs = false;
      }
    });


  }

}
