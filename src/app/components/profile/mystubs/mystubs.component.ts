import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mystubs',
  templateUrl: './mystubs.component.html',
  styleUrls: ['./mystubs.component.css']
})
export class MystubsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }


  editProfile() {
    this.router.navigate(['/profile/profile']);
  }
}
