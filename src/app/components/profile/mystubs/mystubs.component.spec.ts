import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MystubsComponent } from './mystubs.component';

describe('MystubsComponent', () => {
  let component: MystubsComponent;
  let fixture: ComponentFixture<MystubsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MystubsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MystubsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
