import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {
  constructor() { }

   i;
   tabcontent;
   tablinks;
   showtokyo = false;
   showparis = false;
   showlondon = true;
  ngOnInit() {

  }

   openCity(evt, cityName) {

    console.log(evt);
    console.log(cityName);
    if (cityName === 'London') {
      this.showlondon = true;
      this.showparis = false;
      this.showtokyo = false;
    } else if (cityName === 'Paris') {
      this.showparis = true;
      this.showlondon = false;
      this.showtokyo = false;
    } else if ( cityName === 'Tokyo' ) {
      this.showtokyo = true;
      this.showparis = false;
      this.showlondon = false;
    } else {
      this.showlondon = true;
      this.showparis = false;
      this.showtokyo = false;
    }
    // this.tabcontent       =   evt.srcElement.classList;
    // this.tabcontent = document.getElementsByClassName("tabcontent");
    // for (var i = 0; i < this.tabcontent.length; i++) {
    //   this.tabcontent[i].style.display = "none";
    // }
    // tablinks = document.getElementsByClassName("tablinks");
    // for (i = 0; i < tablinks.length; i++) {
    //   tablinks[i].className = tablinks[i].className.replace(" active", "");
    // }
    // document.getElementById(cityName).style.display = "block";
    // evt.currentTarget.className += " active";
  }
  // Get the element with id="defaultOpen" and click on it
  // document.getElementById("defaultOpen").click();  
}

