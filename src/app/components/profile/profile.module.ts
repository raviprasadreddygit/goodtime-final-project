import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {FormsModule , ReactiveFormsModule} from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { SwiperModule } from 'ngx-useful-swiper';

import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { MyOwnCustomMaterialModule } from 'src/app/app.material.module';

import { ContactComponent } from './contact/contact.component';
import { InboxComponent } from './inbox/inbox.component';
import { MystubsComponent } from './mystubs/mystubs.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { ProfileComponent } from './profile/profile.component';
import { TermsComponent } from './terms/terms.component';


export const routes: Routes = [
     { path: 'inbox', component: InboxComponent },
     { path: 'mystubs', component: MystubsComponent },
     { path: 'contact', component: ContactComponent },
     { path: 'privacy', component: PrivacyComponent },
     { path: 'terms', component: TermsComponent },
     { path: 'profile', component: ProfileComponent }
];

@NgModule({
  declarations: [  MystubsComponent, TermsComponent, PrivacyComponent, ContactComponent, InboxComponent, ProfileComponent],
  imports: [
    CommonModule,
    MyOwnCustomMaterialModule,
    AngularFontAwesomeModule,
    FormsModule,
    SwiperModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)

  ]

  // imports: [
  //   RouterModule.forChild(routes)
  // ],
})
export class ProfileModule { }
