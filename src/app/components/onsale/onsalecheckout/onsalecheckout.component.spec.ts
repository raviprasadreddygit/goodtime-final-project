import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnsalecheckoutComponent } from './onsalecheckout.component';

describe('OnsalecheckoutComponent', () => {
  let component: OnsalecheckoutComponent;
  let fixture: ComponentFixture<OnsalecheckoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnsalecheckoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnsalecheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
