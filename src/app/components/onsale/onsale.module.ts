import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {FormsModule , ReactiveFormsModule} from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { SwiperModule } from 'ngx-useful-swiper';

import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { MyOwnCustomMaterialModule } from 'src/app/app.material.module';

import { OnsalecheckoutComponent } from './onsalecheckout/onsalecheckout.component';
import { OnsalecompComponent } from './onsalecomp/onsalecomp.component';
import { OnsaledetailsComponent } from './onsaledetails/onsaledetails.component';
import { OnsaleseatselectionComponent } from './onsaleseatselection/onsaleseatselection.component';

export const routes: Routes = [
  // { path: '', component: LoginComponent },
  { path: 'onsale', component: OnsalecompComponent },
  { path: 'onsaledetails', component: OnsaledetailsComponent },
  { path: 'seatselection', component: OnsaleseatselectionComponent },
  { path: 'onsalecheckout', component: OnsalecheckoutComponent }
];

@NgModule({
  declarations: [  OnsalecompComponent, OnsaledetailsComponent, OnsaleseatselectionComponent, OnsalecheckoutComponent],
  imports: [
    CommonModule,
    MyOwnCustomMaterialModule,
    AngularFontAwesomeModule,
    FormsModule,
    SwiperModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)

  ]

  // imports: [
  //   RouterModule.forChild(routes)
  // ],
})
export class OnsaleModule { }
