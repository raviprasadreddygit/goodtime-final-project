import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnsalecompComponent } from './onsalecomp.component';

describe('OnsalecompComponent', () => {
  let component: OnsalecompComponent;
  let fixture: ComponentFixture<OnsalecompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnsalecompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnsalecompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
