import { Component, OnInit, ViewChild , AfterViewInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatStepper } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-onsaleseatselection',
  templateUrl: './onsaleseatselection.component.html',
  styleUrls: ['./onsaleseatselection.component.css']
})
export class OnsaleseatselectionComponent implements OnInit , AfterViewInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
 
  // @ViewChild('stepper') stepper: MatStepper;
  totalStepsCount: number;

  // tslint:disable-next-line:variable-name
  constructor(private _formBuilder: FormBuilder , private router: Router) {}


  ngOnInit() {
  }



  Checkout() {
    this.router.navigate(['/onsale/onsalecheckout']);
  }



  move(index: number) {
    // this.stepper.selectedIndex = index;
}
ngAfterViewInit() {
  // this.totalStepsCount = this.stepper._steps.length;
}

goBack(stepper: MatStepper) {
  stepper.previous();
}
goForward(stepper: MatStepper) {
  stepper.next();
}

}
