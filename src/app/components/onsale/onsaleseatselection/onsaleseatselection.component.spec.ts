import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnsaleseatselectionComponent } from './onsaleseatselection.component';

describe('OnsaleseatselectionComponent', () => {
  let component: OnsaleseatselectionComponent;
  let fixture: ComponentFixture<OnsaleseatselectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnsaleseatselectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnsaleseatselectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
