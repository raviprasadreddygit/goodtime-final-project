import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnsaledetailsComponent } from './onsaledetails.component';

describe('OnsaledetailsComponent', () => {
  let component: OnsaledetailsComponent;
  let fixture: ComponentFixture<OnsaledetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnsaledetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnsaledetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
