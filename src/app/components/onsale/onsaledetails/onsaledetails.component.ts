import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-onsaledetails',
  templateUrl: './onsaledetails.component.html',
  styleUrls: ['./onsaledetails.component.css']
})
export class OnsaledetailsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }


  SeatSelection() {
    this.router.navigate(['/onsale/seatselection']);
    console.log('okk');
  }

}
