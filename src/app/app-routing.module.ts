import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoodtimeshomeComponent } from './goodtimeshome/goodtimeshome.component';
import { ComingsooncompComponent } from './components/commingsoon/comingsooncomp/comingsooncomp.component';

// import { LoginComponent } from './components/auth/login/login.component';

// /home/manda/goodtimes/goodtimes/src/app/components/auth/auth/auth.module.ts
// /home/manda/goodtimes/goodtimes/src/app/components/auth/auth/auth.module.ts
// /home/manda/goodtimes/goodtimes/src/app/components/onsale/onsale.module.ts
// /home/manda/goodtimes/goodtimes/src/app/components/commingsoon/commingsoon.module.ts

// /home/manda/goodtimes/goodtimes/src/app/components/profile/profile.module.ts
const routes: Routes = [
  {
    path: '',
    redirectTo: 'onsale/onsale',
    pathMatch: 'full'
},
  // { path: '', component: ComingsooncompComponent },
  { path: 'auth', loadChildren: () => import(`./components/auth/auth/auth.module`).then(m => m.AuthModule) }
 , { path: 'onsale', loadChildren: () => import(`./components/onsale/onsale.module`).then(m => m.OnsaleModule) }
 , { path: 'comingsoon', loadChildren: () => import(`./components/commingsoon/commingsoon.module`).then(m => m.CommingSoonModule) }
 , { path: 'profile', loadChildren: () => import(`./components/profile/profile.module`).then(m => m.ProfileModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes ,  { useHash: true })],
exports: [RouterModule]
})
export class AppRoutingModule { }
