import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import {FormsModule , ReactiveFormsModule} from '@angular/forms';

import { NgxCaptchaModule } from 'ngx-captcha';

import { SwiperModule } from 'ngx-useful-swiper';

import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyOwnCustomMaterialModule } from './app.material.module';

import { GoodtimeshomeComponent } from './goodtimeshome/goodtimeshome.component';
import { LoginComponent } from './components/auth/login/login.component';
import { SignupComponent } from './components/auth/signup/signup.component';
// import { ComingsooncompComponent } from './components/commingsoon/comingsooncomp/comingsooncomp.component';


// import { AuthModule } from './components/auth/auth/auth.module';

@NgModule({
  declarations: [
    AppComponent,
    GoodtimeshomeComponent,
    SignupComponent,
    // ComingsooncompComponent,
    LoginComponent
    ],
    entryComponents: [SignupComponent , LoginComponent],

  imports: [
  BrowserModule,
    AppRoutingModule,
    FlexLayoutModule,
    NgxCaptchaModule,
    SwiperModule,
    AngularFontAwesomeModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    MyOwnCustomMaterialModule,
    // AuthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
