import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodtimeshomeComponent } from './goodtimeshome.component';

describe('GoodtimeshomeComponent', () => {
  let component: GoodtimeshomeComponent;
  let fixture: ComponentFixture<GoodtimeshomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodtimeshomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodtimeshomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
