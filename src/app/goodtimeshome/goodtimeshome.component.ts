import { Component, ViewEncapsulation, OnInit, Output, Input } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Router  , ActivatedRoute} from '@angular/router';

import { EventEmitter } from 'protractor';

import { LoginComponent } from '../components/auth/login/login.component';
import { SignupComponent } from '../components/auth/signup/signup.component';
@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-goodtimeshome',
  templateUrl: './goodtimeshome.component.html',
  styleUrls: ['./goodtimeshome.component.css']
})
export class GoodtimeshomeComponent implements OnInit {
  showLoginButton = true;

  showProfile = false;

  CheckLog : boolean;
  // tslint:disable-next-line:new-parens
  showlogs =  true;


  testVal ;
  
  constructor(public dialog: MatDialog  , private router: Router , private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      // tslint:disable-next-line:no-string-literal
      this.showProfile = params['showProfile'];
      // this.showlogs = params['showlogs'];
      console.log(this.showProfile);
      // tslint:disable-next-line:no-string-literal
      // if (this.showProfile === true) {
        // tslint:disable-next-line:no-string-literal
        // this.showlogs = params['showlogs1'];

      if (this.showProfile  === undefined) {
        console.log(localStorage.getItem('showProfileVal'));
        if (localStorage.getItem('showProfileVal') == 'true') {
            this.showProfile = true;
        }
      } else {

        this.testVal = this.showProfile;

        localStorage.setItem('showProfileVal', JSON.parse(this.testVal));
        // console.log('local storage data' , localStorage.getItem('showProfileVal'));
      }

      // this.CheckLog = { sp :  this.showProfile};
      // alert('okk testing');
      // alert(this.CheckLog);
      // localStorage.setItem('showProfileVal', this.showProfile);
      // console.log('local storage data' , localStorage.getItem('showProfileVal'));
      //   // tslint:disable-next-line:no-debugger
      // debugger;

      // }

      // console.log(this.showProfile);
      // console.log(this.showlogs);
      // tslint:disable-next-line:no-debugger
      debugger;
  });

  }




  ngOnInit() {
  // this.showoutPut(this.showProfile);
   console.log(localStorage.getItem('showProfileVal'));
   this.testVal = localStorage.getItem('showProfileVal');
   this.showProfile  =   JSON.parse(this.testVal);
  //  this.showProfile  = localStorage.getItem('showProfileVal');
   console.log('showProfile' , this.showProfile);
  }


  // showoutPut(val) {
  //   console.log(val);
  //   debugger;
  //   this.showProfile = val;
  // }

  // tslint:disable-next-line:member-ordering
  title = 'goodtimes';

  // tslint:disable-next-line:member-ordering
  navbarOpen = false;

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  // openDialog(): void {
  //   const dialogRef = this.dialog.open(SignupComponent, {
  //     width: '640px', disableClose: true
  //   });
// }


openModal() {
  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;
  dialogConfig.data = {
      id: 1,
      title: 'Angular For Beginners'
  };

  const dialogRef = this.dialog.open(SignupComponent, dialogConfig);

  dialogRef.afterClosed().subscribe(result => {
    // alert("response: " + result);
    // console.log(result);
    if (result == undefined || result == null || result == ''){
      this.showProfile = false;
      // this.showlogs = false;
  
    } else {
      this.showProfile = true;
      // this.showlogs = true;

      //  localStorage.setItem('showProfile', this.dataSource.length);

  
    }
  });

  // console.log('ok working.. signup okk');

}


openLoginPage() {
  const dialogloginConfig = new MatDialogConfig();
  dialogloginConfig.disableClose = true;
  dialogloginConfig.autoFocus = true;


  const logindialogRef = this.dialog.open(LoginComponent, dialogloginConfig);

  logindialogRef.afterClosed().subscribe(result => {
    // alert("response: " + result);
    // console.log(result);
    if (result == undefined || result == null || result == ''){
      this.showProfile = false;

      // this.showlogs = false;
    } else {
      this.showProfile = true;
      // this.showlogs = true;
    }
  });

  // console.log('ok working.. login okk');
  // debugger;

}

logout() {
  this.showProfile = false;
  this.showlogs = false;
  localStorage.removeItem("showProfileVal");

  // this.router.navigate(['/']);
}



}
